#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
import logging
import threading

from bot.api_handler import ApiHandler
from config import GROUPS

log = logging.getLogger('vk_bot.main')


def main():
    parser = argparse.ArgumentParser(description='VK Bot for IU8-2k16')
    parser.add_argument(
        '-d', '--debug', action='store_true',
        help='Use this flag to enable debug mode, by default false.',
    )
    parser.add_argument(
        '-t', '--test', action='store_true',
        help='Use this flag to use test group, by default false.',
    )
    args = parser.parse_args()

    for group in GROUPS:
        handler = ApiHandler(**group)
        if handler.is_test == args.test:
            threading.Thread(target=handler.start, kwargs={'debug': args.debug}, daemon=False).start()

    log.debug('All LongPool threads started')


if __name__ == '__main__':
    main()
