import logging
import random
from typing import Optional

from api.messages import MessagesApi
from bot.command import CommandType, Command

log = logging.getLogger('vk_bot.bot.handler.message_edit')


class MessageEditHandler:
    ANSWERS = [
        'Редактируешь значит...',
        'Я не буду смотреть, что ты там написал!',
        'Не, ну это не по правилам!',
        'Первое слово дороже второго!',
        'А вот я всегда всё пишу с первой попытки!',
    ]

    def __init__(self, messages_api: MessagesApi):
        self.__messages_api = messages_api

    def handle(self, obj) -> Optional[Command]:
        peer_id = obj['peer_id']
        text = obj['text']

        if not self.__messages_api.is_for_bot(peer_id, text):
            return

        return Command(
            command_type=CommandType.SEND_MESSAGE,
            peer_id=peer_id,
            message=random.choice(MessageEditHandler.ANSWERS),
        )
