import copy
import datetime
import logging
import random
from typing import Optional, Dict, List

from bot.command import Command, CommandType
from model.congratulation import CongratulationModel, Sex
from model.user import UserModel, User

log = logging.getLogger('vk_bot.bot.handler.birthday')


class BirthdayHandler:
    def __init__(self, users_model: UserModel):
        self.__users_model = users_model

    @staticmethod
    def __get_congratulation(name: str, sex: Sex = Sex.ANY) -> str:
        congratulations = CongratulationModel.get_all(sex)
        congratulation = random.choice(congratulations)
        first_name = name.split()[0]
        return congratulation.template.format(first_name)

    @staticmethod
    def __get_birthday_message(users: Dict[int, User], peer_id: int = None) -> Optional[str]:
        users = copy.copy(users)
        users.pop(peer_id, None)

        if len(users) == 0:
            return None

        number = 0
        users_str = ''
        for user in users.values():
            users_str += user.name
            number += 1

            if len(users) - number > 1:
                users_str += ', '
            elif len(users) - number == 1:
                users_str += ' и '

        if len(users) == 1:
            return f'Сегодня родился один замечательный человечек, {users_str}, так что не забудь поздравить!'
        else:
            return f'Сегодня родились {users_str}, так что не забудь их поздравить!'

    def send_out_birthday(self) -> Optional[List[Command]]:
        log.info('Birthday check started')

        users_list = self.__users_model.birthday_users_get(datetime.date.today())
        if len(users_list) == 0:
            log.info('There is no birthdays :с')
            return None

        users_dict = {}
        for user in users_list:
            users_dict[user.peer_id] = user

        commands = []
        common_peer_ids = []
        common_msg = self.__get_birthday_message(users_dict)

        subscribers = self.__users_model.subscription_info_users()
        for subscriber in subscribers:
            peer_id = subscriber.peer_id

            # If peer not in list of birthday owners
            if peer_id not in users_dict:
                common_peer_ids.append(peer_id)

            else:
                # Message without his name
                private_msg = self.__get_birthday_message(users_dict, peer_id)
                if private_msg is not None:
                    commands.append(Command(
                        command_type=CommandType.SEND_MESSAGE,
                        peer_id=peer_id,
                        message=private_msg,
                    ))

                # Message with congratulation
                congratulation = self.__get_congratulation(users_dict[peer_id].name)
                commands.append(Command(
                    command_type=CommandType.SEND_MESSAGE,
                    peer_id=peer_id,
                    message=congratulation,
                ))

        commands.append(Command(
            command_type=CommandType.SEND_OUT_MESSAGE,
            peer_ids=common_peer_ids,
            message=common_msg,
        ))

        log.info(f"Message '{common_msg}' was sent")
        return commands
