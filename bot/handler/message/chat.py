import logging
import random
from typing import Dict, Any

log = logging.getLogger('vk_bot.bot.handler.message.chat')


class ChatParser:
    def parse(self, clear_text: str, message: Dict[str, Any]):
        if clear_text:
            return random.choice([
                'Я пока не умею общаться на свободные темы :с',
                'Подожди немного, я научусь общаться на любые темы!',
                'Пока я знаю только определнные вещи, просто так болтать не могу :с',
                'Уже скоро я смогу ответить на твое сообщение, а пока я не умею :c',
            ])
