import logging
import random
import re
from enum import Enum
from typing import List, Optional, Match, Dict, Any, Callable, Union, Tuple

from vk_api.keyboard import VkKeyboard, VkKeyboardColor

from api.messages import MessagesApi
from bot.command import CommandType, Command
from model.notices import NoticeModel
from model.user import UserModel, State, User

log = logging.getLogger('vk_bot.bot.handler.message.command')


class PattenType(Enum):
    CLEAR_WORD = 0
    WORD = 1
    FULL = 2


class MsgCommand:
    def __init__(self, callback: Callable[[Dict[str, Any], bool, Optional[Match[str]]], Union[Command, List[Command]]],
                 pattens: List[str], pattens_type: PattenType,
                 for_dialog: bool = True, for_chat: bool = True, for_admin: bool = False,
                 description: Optional[str] = None, button: Optional[Tuple[str, VkKeyboardColor]] = None):
        self.__callback = callback
        self.for_dialog = for_dialog
        self.for_chat = for_chat
        self.for_admin = for_admin
        self.description = description
        self.button = button
        self.__patterns = []
        for pattern in pattens:
            if pattens_type == PattenType.CLEAR_WORD:
                pattern = fr'^(.*\W)?({pattern})(\W.*)?$'
            elif pattens_type == PattenType.WORD:
                pattern = fr'^(.*\W)?({pattern}\w{{0,2}})(\W.*)?$'
            elif pattens_type == pattens_type.FULL:
                pattern = fr'^({pattern})$'
            else:
                pattern = fr'({pattern})'
            self.__patterns.append(re.compile(pattern, re.IGNORECASE | re.UNICODE))

    def check_pattern(self, message: str) -> Optional[Match[str]]:
        for pattern in self.__patterns:
            res = pattern.search(message)
            if res is not None:
                return res
        return None

    def execute(self, message: Dict[str, Any], is_chat: bool, match_res: Optional[Match[str]] = None):
        return self.__callback(message, is_chat, match_res)


class CommandParser:
    def __init__(self, messages_api: MessagesApi, users_model: UserModel, notices_model: NoticeModel,
                 important_chat_id: int, spam_chat_id: int, men_chat_id: int, vkpay_chat_id: int):
        self.__messages_api = messages_api
        self.__users_model = users_model
        self.__notices_model = notices_model

        self.__important_chat_id = important_chat_id
        self.__spam_chat_id = spam_chat_id
        self.__men_chat_id = men_chat_id
        self.__vkpay_chat_id = vkpay_chat_id

        self.__commands = {
            State.DEFAULT: [
                MsgCommand(
                    callback=self.command_ping,
                    pattens=[r'ping', r'пинг'],
                    pattens_type=PattenType.FULL,
                ),
                MsgCommand(
                    callback=self.command_pong,
                    pattens=[r'pong', r'понг'],
                    pattens_type=PattenType.FULL,
                ),
                MsgCommand(
                    callback=self.command_start,
                    pattens=[r'start', r'begin', r'старт', r'начать', r'начинай(те)?'],
                    pattens_type=PattenType.FULL,
                    for_chat=False,
                ),
                MsgCommand(
                    callback=self.command_subscribe,
                    pattens=[r'(включи(ть)?|подпи(сать(ся)?|ши))( на)?( рассылк.)?'],
                    pattens_type=PattenType.FULL,
                    description='Включить/выключить рассылку постов/мемов: подпиши на посты',
                    button=('Подпиши', VkKeyboardColor.PRIMARY),
                ),
                MsgCommand(
                    callback=self.command_unsubscribe,
                    pattens=[r'((от|вы)ключи(ть)?|отпи(сать(ся)?|ши))( от)?( рассылк.)?'],
                    pattens_type=PattenType.FULL,
                    button=('Отпиши', VkKeyboardColor.PRIMARY),
                ),
                MsgCommand(
                    callback=self.command_post_subscribe,
                    pattens=[r'(включи(ть)? |подпи(сать(ся)?|ши) )(на )?(рассылк. )?пост\w{0,2}'],
                    pattens_type=PattenType.FULL,
                ),
                MsgCommand(
                    callback=self.command_post_unsubscribe,
                    pattens=[r'((от|вы)ключи(ть)? |отпи(сать(ся)?|ши) )(от )?(рассылк. )?пост\w{0,2}'],
                    pattens_type=PattenType.FULL,
                ),
                MsgCommand(
                    callback=self.command_mem_subscribe,
                    pattens=[r'(включи(ть)? |подпи(сать(ся)?|ши) )(на )?(рассылк. )?мем(ас|ч)?(ик)?\w{0,2}'],
                    pattens_type=PattenType.FULL,
                ),
                MsgCommand(
                    callback=self.command_mem_unsubscribe,
                    pattens=[r'((от|вы)ключи(ть)? |отпи(сать(ся)?|ши) )(от )?(рассылк. )?мем(ас|ч)?(ик)?\w{0,2}'],
                    pattens_type=PattenType.FULL,
                ),
                MsgCommand(
                    callback=self.command_notice_subscribe,
                    pattens=[r'(включи(ть)? |подпи(сать(ся)?|ши) )(на )?(рассылк. )?(уведомлен|д(е|э)длайн)\w{0,2}'],
                    pattens_type=PattenType.FULL,
                ),
                MsgCommand(
                    callback=self.command_notice_unsubscribe,
                    pattens=[
                        r'((от|вы)ключи(ть)? |отпи(сать(ся)?|ши) )(от )?(рассылк. )?(уведомлен|д(е|э)длайн)\w{0,2}'],
                    pattens_type=PattenType.FULL,
                ),
                MsgCommand(
                    callback=self.command_send_out_info_start,
                    pattens=[
                        r'(send\s*)?info(\s*send)?',
                        r'((разо|пере)(шли|слать)|скинь)?\s*инф(ормац)?\w{1,2}',
                    ],
                    pattens_type=PattenType.FULL,
                    for_chat=False,
                    for_admin=True,
                    description='Разослать информацию: (разошли) инфу',
                    button=('Разошли инфу', VkKeyboardColor.SECONDARY),
                ),
                MsgCommand(
                    callback=self.command_send_out_notice_start,
                    pattens=[
                        r'(send\s*)?info(\s*send)?',
                        r'((разо|пере)(шли|слать)|скинь)?\s*(уведомлен|д(е|э)длайн)\w{0,2}',
                    ],
                    pattens_type=PattenType.FULL,
                    for_chat=False,
                    for_admin=True,
                    description='Разослать информацию о дедлайне: (разошли) дедлайн',
                    button=('Разошли дедлайн', VkKeyboardColor.SECONDARY),
                ),
                MsgCommand(
                    callback=self.command_send_out_mem_start,
                    pattens=[
                        r'(send\s*)?mem(\s*send)?',
                        r'((разо|пере)(шли|слать)|скинь)?\s*(мем(ас|ч)?(ик)?|смешнявк.)',
                    ],
                    pattens_type=PattenType.FULL,
                    for_chat=False,
                    description='Разослать мем, подписанным на рассылку: (разошли) мем',
                    button=('Разошли мем', VkKeyboardColor.SECONDARY),
                ),
                MsgCommand(
                    callback=self.command_reply_to_important_start,
                    pattens=[
                        r'(reply\s*)?important(\s*reply)?',
                        r'((пере(шли|слать)|напи(ши|сать)|скинь)\s*в?\s*)?важное',
                    ],
                    pattens_type=PattenType.FULL,
                    for_chat=False,
                    for_admin=True,
                    description='Отправка сообщения в беседу: перешли в важное',
                    button=('Напиши в важное', VkKeyboardColor.SECONDARY),
                ),
                MsgCommand(
                    callback=self.command_reply_to_spam_start,
                    pattens=[
                        r'(reply\s*)?(spam|chat)(\s*reply)?',
                        r'((пере(шли|слать)|напи(ши|сать)|скинь)\s*в?\s*)?(бесед.|чат)',
                    ],
                    pattens_type=PattenType.FULL,
                    for_chat=False,
                    description='Отправка сообщения в беседу: перешли в беседу',
                    button=('Напиши в беседу', VkKeyboardColor.SECONDARY),
                ),
                MsgCommand(
                    callback=self.command_reply_to_men_start,
                    pattens=[
                        r'(reply\s*)?m(a|e)n(\s*reply)?',
                        r'((пере(шли|слать)|напи(ши|сать)|скинь)\s*в?\s*)?без\s*баб',
                    ],
                    pattens_type=PattenType.FULL,
                    for_chat=False,
                    description='Отправка сообщения в беседу без баб: перешли в без баб',
                    button=('Напиши в без баб', VkKeyboardColor.SECONDARY),
                ),
                MsgCommand(
                    callback=self.command_next_lvl,
                    pattens=[r'next[_\s]?le?ve?l', r'н(е|э)кст[_\s]*л(е|э)?в(е|э)?л'],
                    pattens_type=PattenType.FULL,
                    description='Отправить "The next level play!" в чат: НЭКСТЛВЛ',
                ),
                MsgCommand(
                    callback=self.command_coin,
                    pattens=[r'(подбрось|подкинь)\s+монет(к)?у', r'монет(к)?а', r'орел или решка', r'решка или орел'],
                    pattens_type=PattenType.FULL,
                    description='Можно подбросить монетку!',
                ),
                MsgCommand(
                    callback=self.command_felix,
                    pattens=[
                        r'ф(е|э)ликс', r'фл(е|э)кс', r'ф(е|э)ликс[ -]*фл(е|э)кс', r'фл(е|э)кс[ -]*ф(е|э)ликс', r'фз',
                    ],
                    pattens_type=PattenType.FULL,
                    # description='А что ТЫ знаешь о Феликсе?',
                ),
                MsgCommand(
                    callback=self.command_laugh,
                    pattens=[r'(по)?угар', r'(по)?смейся', r'залуп'],
                    pattens_type=PattenType.WORD,
                ),
                MsgCommand(
                    callback=self.command_kononova,
                    pattens=[r'кон+о[но]+в'],
                    pattens_type=PattenType.WORD,
                ),
                MsgCommand(
                    callback=self.command_labka,
                    pattens=[r'лабк'],
                    pattens_type=PattenType.WORD,
                ),
                MsgCommand(
                    callback=self.command_go,
                    pattens=[r'(идти|пойдем|пойдет|пойдут|прий?дет|прий?дут|пришел|пришла|пойдешь|прий?дешь).*\?'],
                    pattens_type=PattenType.CLEAR_WORD,
                ),
                MsgCommand(
                    callback=self.command_gg,
                    pattens=[r'гг', r'gg'],
                    pattens_type=PattenType.CLEAR_WORD,
                ),
                MsgCommand(
                    callback=self.command_admin,
                    pattens=[r'(режим )?админ.?', r'хочу поговорить с админом', r'(бот )?за(ткнись|молкни)'],
                    pattens_type=PattenType.FULL,
                    for_chat=False,
                    description='Могу заткнуться и не отвечать, это будут делать только админы: режим админа',
                    button=('Режим админа', VkKeyboardColor.NEGATIVE),
                ),
                MsgCommand(
                    callback=self.command_help,
                    pattens=[
                        r'помощ', r'п(о|а)м(о|а)ги(т(е|и))?', r'х(е|э)лп(ани)?', r'паник', r'что.*умешь?', r'команд'
                    ],
                    pattens_type=PattenType.WORD,
                    for_chat=False,
                    description='Вывод списка команд (этот список): любой призыв о помощи',
                    button=('Помощь', VkKeyboardColor.POSITIVE),
                ),
                MsgCommand(
                    callback=self.command_help,
                    pattens=[r'help', r'panic'],
                    pattens_type=PattenType.WORD,
                ),
                # MsgCommand(
                #     callback=self.command_stirlitz,
                #     pattens=[r'штирл(и|е)ц'],
                #     pattens_type=PattenType.WORD,
                #     description='Как насчёт анекдотов про Штирлица?',
                # ),
            ],
            State.ADMIN_MODE: [
                MsgCommand(
                    callback=self.command_bot_on,
                    pattens=[r'выйти', r'выход', r'стоп', r'(включить )?бот.?( вернись)?'],
                    pattens_type=PattenType.FULL,
                ),
                MsgCommand(
                    callback=self.command_skip,
                    pattens=[r'.*'],
                    pattens_type=PattenType.FULL,
                ),
            ],
            State.SUBSCRIBE: [
                MsgCommand(
                    callback=self.command_cancel,
                    pattens=[r'cancel', r'ничего', r'отмен.(ть)?'],
                    pattens_type=PattenType.FULL,
                ),
                MsgCommand(
                    callback=self.command_post_subscribe,
                    pattens=[r'1', r'(на\s*)?пост\w{0,2}', r'(на\s*)?групп?\w{0,2}'],
                    pattens_type=PattenType.FULL,
                ),
                MsgCommand(
                    callback=self.command_mem_subscribe,
                    pattens=[r'2', r'(на\s*)?мем(ас|ч)?(ик)?\w{0,2}'],
                    pattens_type=PattenType.FULL,
                ),
                MsgCommand(
                    callback=self.command_notice_subscribe,
                    pattens=[r'3', r'(на\s*)?(уведомлен|д(е|э)длайн)\w{0,2}'],
                    pattens_type=PattenType.FULL,
                ),
                MsgCommand(
                    callback=self.command_retry,
                    pattens=[r'.*'],
                    pattens_type=PattenType.FULL,
                ),
            ],
            State.UNSUBSCRIBE: [
                MsgCommand(
                    callback=self.command_cancel,
                    pattens=[r'cancel', r'ничего', r'отмен.(ть)?'],
                    pattens_type=PattenType.FULL,
                ),
                MsgCommand(
                    callback=self.command_post_unsubscribe,
                    pattens=[r'1', r'(от\s*)?пост\w{0,2}', r'(от\s*)?групп?\w{0,2}'],
                    pattens_type=PattenType.FULL,
                ),
                MsgCommand(
                    callback=self.command_mem_unsubscribe,
                    pattens=[r'2', r'(от\s*)?мем(ас|ч)?(ик)?\w{0,2}'],
                    pattens_type=PattenType.FULL,
                ),
                MsgCommand(
                    callback=self.command_notice_unsubscribe,
                    pattens=[r'3', r'(от\s*)?(уведомлен|д(е|э)длайн)\w{0,2}'],
                    pattens_type=PattenType.FULL,
                ),
                MsgCommand(
                    callback=self.command_retry,
                    pattens=[r'.*'],
                    pattens_type=PattenType.FULL,
                ),
            ],
            State.REPLY_IMPORTANT: [
                MsgCommand(
                    callback=self.command_cancel,
                    pattens=[r'cancel', r'ничего', r'отмен.(ть)?'],
                    pattens_type=PattenType.FULL,
                ),
                MsgCommand(
                    callback=self.command_reply_to_important,
                    pattens=[r'.*'],
                    pattens_type=PattenType.FULL,
                ),
            ],
            State.REPLY_SPAM: [
                MsgCommand(
                    callback=self.command_cancel,
                    pattens=[r'cancel', r'ничего', r'отмен.(ть)?'],
                    pattens_type=PattenType.FULL,
                ),
                MsgCommand(
                    callback=self.command_reply_to_spam,
                    pattens=[r'.*'],
                    pattens_type=PattenType.FULL,
                ),
            ],
            State.REPLY_MEN: [
                MsgCommand(
                    callback=self.command_cancel,
                    pattens=[r'cancel', r'ничего', r'отмен.(ть)?'],
                    pattens_type=PattenType.FULL,
                ),
                MsgCommand(
                    callback=self.command_reply_to_men,
                    pattens=[r'.*'],
                    pattens_type=PattenType.FULL,
                ),
            ],
            State.REPLY_VKPAY: [
                MsgCommand(
                    callback=self.command_cancel,
                    pattens=[r'cancel', r'ничего', r'отмен.(ть)?'],
                    pattens_type=PattenType.FULL,
                ),
                MsgCommand(
                    callback=self.command_reply_to_vkpay,
                    pattens=[r'.*'],
                    pattens_type=PattenType.FULL,
                ),
            ],
            State.SEND_OUT_INFO: [
                MsgCommand(
                    callback=self.command_cancel,
                    pattens=[r'cancel', r'ничего', r'отмен.(ть)?'],
                    pattens_type=PattenType.FULL,
                ),
                MsgCommand(
                    callback=self.command_send_out_info,
                    pattens=[r'.*'],
                    pattens_type=PattenType.FULL,
                ),
            ],
            State.SEND_OUT_MEM: [
                MsgCommand(
                    callback=self.command_cancel,
                    pattens=[r'cancel', r'ничего', r'отмен.(ть)?'],
                    pattens_type=PattenType.FULL,
                ),
                MsgCommand(
                    callback=self.command_send_out_mem,
                    pattens=[r'.*'],
                    pattens_type=PattenType.FULL,
                ),
            ],
            State.SEND_OUT_NOTICE: [
                MsgCommand(
                    callback=self.command_cancel,
                    pattens=[r'cancel', r'ничего', r'отмен.(ть)?'],
                    pattens_type=PattenType.FULL,
                ),
                MsgCommand(
                    callback=self.command_send_out_notice,
                    pattens=[r'.*'],
                    pattens_type=PattenType.FULL,
                ),
            ],
            State.SELECT_NOTICE_TIME: [
                MsgCommand(
                    callback=self.command_cancel,
                    pattens=[r'cancel', r'никогда', r'отмен.(ть)?'],
                    pattens_type=PattenType.FULL,
                ),
                MsgCommand(
                    callback=self.command_select_notice_time,
                    pattens=[r'.*'],
                    pattens_type=PattenType.FULL,
                ),
            ],
            State.SAVE_NOTICE: [
                MsgCommand(
                    callback=self.command_cancel,
                    pattens=[r'cancel', r'ничего', r'отмен.(ть)?'],
                    pattens_type=PattenType.FULL,
                ),
                MsgCommand(
                    callback=self.command_save_notice,
                    pattens=[r'.*'],
                    pattens_type=PattenType.FULL,
                ),
            ],
        }

        for state, commands in self.__commands.items():
            commands.insert(0, MsgCommand(
                callback=self.command_debug,
                pattens=[r'debug', r'дебаг'],
                pattens_type=PattenType.FULL,
            ))

    def get_default_keyboard(self, peer_id: bool) -> str:
        if MessagesApi.is_chat(peer_id):
            return ''

        user = self.__users_model.get(peer_id)

        keyboard = VkKeyboard()
        line_count = 0
        for command in self.__commands[State.DEFAULT]:
            if command.button is not None and command.for_dialog:
                if not command.for_admin or user.is_admin:
                    if line_count == 2:
                        line_count = 0
                        keyboard.add_line()
                    keyboard.add_button(command.button[0], command.button[1])
                    line_count += 1
        return keyboard.get_keyboard()

    @staticmethod
    def command_skip(message: Dict[str, Any], is_chat: bool, match_res: Optional[Match[str]]):
        return []

    def command_ping(self, message: Dict[str, Any], is_chat: bool, match_res: Optional[Match[str]]):
        peer_id = message['peer_id']
        return Command(
            command_type=CommandType.SEND_MESSAGE,
            peer_id=peer_id,
            keyboard=self.get_default_keyboard(peer_id),
            message='Pong!',
        )

    def command_pong(self, message: Dict[str, Any], is_chat: bool, match_res: Optional[Match[str]]):
        peer_id = message['peer_id']
        return Command(
            command_type=CommandType.SEND_MESSAGE,
            peer_id=peer_id,
            keyboard=self.get_default_keyboard(peer_id),
            message=random.choice([
                'А вот нифига не пинг!',
                'Умный слишком?',
                'Ну допустим "пинг"',
            ]),
        )

    def command_start(self, message: Dict[str, Any], is_chat: bool, match_res: Optional[Match[str]]):
        peer_id = message['peer_id']
        first_name = self.__users_model.get(peer_id).name.split()[0]
        text = (
            f'Привет, {first_name}!<br>'
            'Меня зовут Паня (просто мои создатели любят команду "panic" из одного хорошего языка программирования). '
            'Я сделан для того, чтобы тебе было проще учиться. '
            'Для начала можешь написать "помощь" или что-то в этом роде, чтобы узнать, что я умею. '
            'Не забудь подписаться на посты и мемы!'
        )
        return Command(
            command_type=CommandType.SEND_MESSAGE,
            peer_id=peer_id,
            keyboard=self.get_default_keyboard(peer_id),
            message=text,
        )

    def command_subscribe(self, message: Dict[str, Any], is_chat: bool, match_res: Optional[Match[str]]):
        peer_id = message['peer_id']
        self.__users_model.state_set(peer_id, State.SUBSCRIBE)

        keyboard = VkKeyboard()
        keyboard.add_button('На посты')
        keyboard.add_button('На мемы')
        keyboard.add_button('На уведомления')

        return Command(
            command_type=CommandType.SEND_MESSAGE,
            peer_id=peer_id,
            keyboard=keyboard.get_keyboard(),
            message='На что хочешь подписаться? Просто введи номер.<br>'
                    '1. На посты<br>'
                    '2. На мемы<br>'
                    '3. На уведомления и дедлайны',
        )

    def command_unsubscribe(self, message: Dict[str, Any], is_chat: bool, match_res: Optional[Match[str]]):
        peer_id = message['peer_id']
        self.__users_model.state_set(peer_id, State.UNSUBSCRIBE)

        keyboard = VkKeyboard()
        keyboard.add_button('От постов')
        keyboard.add_button('От мемов')
        keyboard.add_button('От уведомлений')

        return Command(
            command_type=CommandType.SEND_MESSAGE,
            peer_id=peer_id,
            keyboard=keyboard.get_keyboard(),
            message='От чего хочешь отписаться? Просто введи номер.<br>'
                    '1. От постов<br>'
                    '2. От мемов<br>'
                    '3. От уведомлений и дедлайнов',
        )

    def command_post_subscribe(self, message: Dict[str, Any], is_chat: bool, match_res: Optional[Match[str]]):
        peer_id = message['peer_id']

        self.__users_model.state_set(peer_id, State.DEFAULT)
        self.__users_model.subscription_post_set(peer_id, True)

        return Command(
            command_type=CommandType.SEND_MESSAGE,
            peer_id=peer_id,
            keyboard=self.get_default_keyboard(peer_id),
            message='Подписал на посты!',
        )

    def command_post_unsubscribe(self, message: Dict[str, Any], is_chat: bool, match_res: Optional[Match[str]]):
        peer_id = message['peer_id']

        self.__users_model.state_set(peer_id, State.DEFAULT)
        self.__users_model.subscription_post_set(peer_id, False)

        return Command(
            command_type=CommandType.SEND_MESSAGE,
            peer_id=peer_id,
            keyboard=self.get_default_keyboard(peer_id),
            message='Отписал от постов! Если что, всегда можно подписаться заново)',
        )

    def command_mem_subscribe(self, message: Dict[str, Any], is_chat: bool, match_res: Optional[Match[str]]):
        peer_id = message['peer_id']

        self.__users_model.state_set(peer_id, State.DEFAULT)
        self.__users_model.subscription_mem_set(peer_id, True)

        return Command(
            command_type=CommandType.SEND_MESSAGE,
            peer_id=peer_id,
            keyboard=self.get_default_keyboard(peer_id),
            message='Подписал на мемы!',
        )

    def command_mem_unsubscribe(self, message: Dict[str, Any], is_chat: bool, match_res: Optional[Match[str]]):
        peer_id = message['peer_id']

        self.__users_model.state_set(peer_id, State.DEFAULT)
        self.__users_model.subscription_mem_set(peer_id, False)

        return Command(
            command_type=CommandType.SEND_MESSAGE,
            peer_id=peer_id,
            keyboard=self.get_default_keyboard(peer_id),
            message='Отписал от мемов! Если что, всегда можно подписаться заново)',
        )

    def command_notice_subscribe(self, message: Dict[str, Any], is_chat: bool, match_res: Optional[Match[str]]):
        peer_id = message['peer_id']

        self.__users_model.state_set(peer_id, State.DEFAULT)
        self.__users_model.subscription_notice_set(peer_id, True)

        return Command(
            command_type=CommandType.SEND_MESSAGE,
            peer_id=peer_id,
            keyboard=self.get_default_keyboard(peer_id),
            message='Подписал на уведомения!',
        )

    def command_notice_unsubscribe(self, message: Dict[str, Any], is_chat: bool, match_res: Optional[Match[str]]):
        peer_id = message['peer_id']

        self.__users_model.state_set(peer_id, State.DEFAULT)
        self.__users_model.subscription_notice_set(peer_id, False)

        return Command(
            command_type=CommandType.SEND_MESSAGE,
            peer_id=peer_id,
            keyboard=self.get_default_keyboard(peer_id),
            message='Отписал от уведомений! Если что, всегда можно подписаться заново)',
        )

    def command_send_out_info_start(self, message: Dict[str, Any], is_chat: bool, match_res: Optional[Match[str]]):
        peer_id = message['peer_id']

        self.__users_model.state_set(peer_id, State.SEND_OUT_INFO)

        keyboard = VkKeyboard()
        keyboard.add_button('Ничего', VkKeyboardColor.NEGATIVE)

        return Command(
            command_type=CommandType.SEND_MESSAGE,
            peer_id=peer_id,
            keyboard=keyboard.get_keyboard(),
            message='Какую информацию хочешь разослать? Напиши "ничего", чтобы ничего не рассылать.',
        )

    def command_send_out_notice_start(self, message: Dict[str, Any], is_chat: bool, match_res: Optional[Match[str]]):
        peer_id = message['peer_id']

        self.__users_model.state_set(peer_id, State.SELECT_NOTICE_TIME)

        keyboard = VkKeyboard()
        keyboard.add_button('Сейчас', VkKeyboardColor.SECONDARY)
        keyboard.add_button('Никогда', VkKeyboardColor.NEGATIVE)

        return Command(
            command_type=CommandType.SEND_MESSAGE,
            peer_id=peer_id,
            keyboard=keyboard.get_keyboard(),
            message='Когда хочешь разослать информацию о дедлайне? '
                    'Напиши в формате гггг.мм.дд ЧЧ:ММ н (год, месяц, день, час, минута, день недели), "*" = любой. '
                    'Напиши "ничего", чтобы ничего не рассылать.',
        )

    def command_send_out_mem_start(self, message: Dict[str, Any], is_chat: bool, match_res: Optional[Match[str]]):
        peer_id = message['peer_id']
        self.__users_model.state_set(peer_id, State.SEND_OUT_MEM)

        keyboard = VkKeyboard()
        keyboard.add_button('Ничего', VkKeyboardColor.NEGATIVE)

        return Command(
            command_type=CommandType.SEND_MESSAGE,
            peer_id=peer_id,
            keyboard=keyboard.get_keyboard(),
            message='Какой мем хочешь разослать? Напиши "ничего", чтобы ничего не рассылать.',
        )

    def command_reply_to_important_start(self, message: Dict[str, Any], is_chat: bool, match_res: Optional[Match[str]]):
        peer_id = message['peer_id']

        self.__users_model.state_set(peer_id, State.REPLY_IMPORTANT)

        keyboard = VkKeyboard()
        keyboard.add_button('Ничего', VkKeyboardColor.NEGATIVE)

        return Command(
            command_type=CommandType.SEND_MESSAGE,
            peer_id=peer_id,
            keyboard=keyboard.get_keyboard(),
            message='Что хочешь переслать в важное? Напиши "ничего", чтобы ничего не пересылать.',
        )

    def command_reply_to_spam_start(self, message: Dict[str, Any], is_chat: bool, match_res: Optional[Match[str]]):
        peer_id = message['peer_id']
        self.__users_model.state_set(peer_id, State.REPLY_SPAM)

        keyboard = VkKeyboard()
        keyboard.add_button('Ничего', VkKeyboardColor.NEGATIVE)

        return Command(
            command_type=CommandType.SEND_MESSAGE,
            peer_id=peer_id,
            keyboard=keyboard.get_keyboard(),
            message='Что хочешь переслать в беседу? Напиши "ничего", чтобы ничего не пересылать.',
        )

    def command_reply_to_men_start(self, message: Dict[str, Any], is_chat: bool, match_res: Optional[Match[str]]):
        peer_id = message['peer_id']
        self.__users_model.state_set(peer_id, State.REPLY_MEN)

        keyboard = VkKeyboard()
        keyboard.add_button('Ничего', VkKeyboardColor.NEGATIVE)

        return Command(
            command_type=CommandType.SEND_MESSAGE,
            peer_id=peer_id,
            keyboard=keyboard.get_keyboard(),
            message='Что хочешь переслать в беседу без баб? Напиши "ничего", чтобы ничего не пересылать.',
        )

    def command_next_lvl(self, message: Dict[str, Any], is_chat: bool, match_res: Optional[Match[str]]):
        peer_id = message['peer_id']
        return Command(
            command_type=CommandType.SEND_MESSAGE,
            peer_id=peer_id,
            keyboard=self.get_default_keyboard(peer_id),
            attachment=MessagesApi.audio(-56333679, 456239058),
        )

    def command_coin(self, message: Dict[str, Any], is_chat: bool, match_res: Optional[Match[str]]):
        peer_id = message['peer_id']
        text = 'Орёл!' if random.randint(0, 1) == 0 else 'Решка!'
        return Command(
            command_type=CommandType.SEND_MESSAGE,
            peer_id=peer_id,
            keyboard=self.get_default_keyboard(peer_id),
            message=text,
            attachment=MessagesApi.audio(371745463, 456318461),
        )

    def command_felix(self, message: Dict[str, Any], is_chat: bool, match_res: Optional[Match[str]]):
        peer_id = message['peer_id']
        return Command(
            command_type=CommandType.SEND_MESSAGE,
            peer_id=peer_id,
            keyboard=self.get_default_keyboard(peer_id),
            attachment=[
                MessagesApi.doc(41644498, 497661870),
                MessagesApi.audio(371745441, 456289076),
            ]
        )

    def command_laugh(self, message: Dict[str, Any], is_chat: bool, match_res: Optional[Match[str]]):
        peer_id = message['peer_id']
        return Command(
            command_type=CommandType.SEND_MESSAGE,
            peer_id=peer_id,
            keyboard=self.get_default_keyboard(peer_id),
            attachment=MessagesApi.audio(41644498, 456239218),
        )

    def command_kononova(self, message: Dict[str, Any], is_chat: bool, match_res: Optional[Match[str]]):
        peer_id = message['peer_id']
        return Command(
            command_type=CommandType.SEND_MESSAGE,
            peer_id=peer_id,
            keyboard=self.get_default_keyboard(peer_id),
            message='Эй, пиздюк, вообще-то Коннова!',
        )

    def command_labka(self, message: Dict[str, Any], is_chat: bool, match_res: Optional[Match[str]]):
        peer_id = message['peer_id']
        return Command(
            command_type=CommandType.SEND_MESSAGE,
            peer_id=peer_id,
            keyboard=self.get_default_keyboard(peer_id),
            message='Ещё раз напишешь "лабка", я тебе её куда-нибудь засуну!',
        )

    def command_go(self, message: Dict[str, Any], is_chat: bool, match_res: Optional[Match[str]]):
        peer_id = message['peer_id']
        word = match_res[3].lower()
        if word[-3:] == 'ешь':
            word = f'{word[:-3]}ет'
        if random.randint(0, 1) == 0:
            text = random.choice([
                f'Определенно {word}!',
                f'Да, скорее всего {word}',
                f'Разумеется {word}!',
                f'Более чем уверен, что {word}',
                f'Да конечно же {word}!',
            ])
        else:
            text = random.choice([
                f'Да не, не {word}',
                f'Нууууу, мне кажется, что не {word}..',
                f'Думаю, что не {word}..',
                f'Звёзды говорят, что не {word}!',
                f'Да зачем.. Не {word} конечно же!',
            ])
        return Command(
            command_type=CommandType.SEND_MESSAGE,
            peer_id=peer_id,
            keyboard=self.get_default_keyboard(peer_id),
            message=text,
        )

    def command_gg(self, message: Dict[str, Any], is_chat: bool, match_res: Optional[Match[str]]):
        peer_id = message['peer_id']
        return Command(
            command_type=CommandType.SEND_MESSAGE,
            peer_id=peer_id,
            keyboard=self.get_default_keyboard(peer_id),
            message='WP!',
        )

    def command_admin(self, message: Dict[str, Any], is_chat: bool, match_res: Optional[Match[str]]):
        peer_id = message['peer_id']
        self.__users_model.state_set(peer_id, State.ADMIN_MODE)

        keyboard = VkKeyboard()
        keyboard.add_button('Включить бота', VkKeyboardColor.POSITIVE)

        return Command(
            command_type=CommandType.SEND_MESSAGE,
            peer_id=peer_id,
            keyboard=keyboard.get_keyboard(),
            message='Теперь тебе будут отвечать только админы. Чтобы бот начал отвечать, напиши "бот" или "выйти"',
        )

    def command_help(self, message: Dict[str, Any], is_chat: bool, match_res: Optional[Match[str]]):
        peer_id = message['peer_id']
        state = self.__users_model.state_get(peer_id)
        user = self.__users_model.get(peer_id)

        text = 'Вот что я умею тут делать:'
        for command in self.__commands[state]:
            if command.description is not None:
                if (not is_chat and command.for_dialog) or (is_chat and command.for_chat):
                    if not command.for_admin or user.is_admin:
                        text += f'<br>- {command.description}'
        text += '<br>Также я иногда вставляю свое слово в некоторых ситуациях)'

        return Command(
            command_type=CommandType.SEND_MESSAGE,
            peer_id=peer_id,
            keyboard=self.get_default_keyboard(peer_id),
            message=text,
        )

    def command_bot_on(self, message: Dict[str, Any], is_chat: bool, match_res: Optional[Match[str]]):
        peer_id = message['peer_id']
        self.__users_model.state_set(peer_id, State.DEFAULT)

        return Command(
            command_type=CommandType.SEND_MESSAGE,
            peer_id=peer_id,
            keyboard=self.get_default_keyboard(peer_id),
            message='Всё, теперь ты снова можешь болтать со мной)',
        )

    def command_cancel(self, message: Dict[str, Any], is_chat: bool, match_res: Optional[Match[str]]):
        peer_id = message['peer_id']
        self.__users_model.state_set(peer_id, State.DEFAULT)

        return Command(
            command_type=CommandType.SEND_MESSAGE,
            peer_id=peer_id,
            keyboard=self.get_default_keyboard(peer_id),
            message='Окей. Пиши, если что 😉',
        )

    def command_retry(self, message: Dict[str, Any], is_chat: bool, match_res: Optional[Match[str]]):
        peer_id = message['peer_id']
        self.__users_model.state_set(peer_id, State.DEFAULT)

        return Command(
            command_type=CommandType.SEND_MESSAGE,
            peer_id=peer_id,
            message='Что-то не понятно, попробуй еще раз!',
        )

    def __reply(self, message: Dict[str, Any], chat_id: int):
        peer_id = message['peer_id']
        text = message['text']
        attachments = message['attachments']
        new_attachments = self.__messages_api.reply_attachments(attachments)

        if not text and not new_attachments:
            return []

        self.__users_model.state_set(peer_id, State.DEFAULT)

        return [
            Command(
                command_type=CommandType.SEND_MESSAGE,
                peer_id=peer_id,
                keyboard=self.get_default_keyboard(peer_id),
                message='Пересылаю! Пиши, если что 😉',
            ),
            Command(
                command_type=CommandType.SEND_MESSAGE,
                peer_id=chat_id,
                message=text,
                attachment=new_attachments,
            ),
        ]

    def command_reply_to_important(self, message: Dict[str, Any], is_chat: bool, match_res: Optional[Match[str]]):
        peer_id = message['peer_id']
        commands = self.__reply(message, self.__important_chat_id)
        if commands:
            log.warning(f'{peer_id} написал в важное')
        return commands

    def command_reply_to_spam(self, message: Dict[str, Any], is_chat: bool, match_res: Optional[Match[str]]):
        peer_id = message['peer_id']
        commands = self.__reply(message, self.__spam_chat_id)
        if commands:
            log.info(f'{peer_id} написал в беседу')
        return commands

    def command_reply_to_men(self, message: Dict[str, Any], is_chat: bool, match_res: Optional[Match[str]]):
        peer_id = message['peer_id']
        commands = self.__reply(message, self.__men_chat_id)
        if commands:
            log.info(f'{peer_id} написал в беседу без баб')
        return commands

    def command_reply_to_vkpay(self, message: Dict[str, Any], is_chat: bool, match_res: Optional[Match[str]]):
        peer_id = message['peer_id']
        commands = self.__reply(message, self.__vkpay_chat_id)
        if commands:
            log.warning(f'{peer_id} написал в "оплачиваемую" беседу')
        return commands

    def __send_out(self, message: Dict[str, Any], subscribers: List[User]):
        peer_id = message['peer_id']
        text = message['text']
        attachments = message['attachments']
        new_attachments = self.__messages_api.reply_attachments(attachments)

        peer_ids = []
        for subscriber in subscribers:
            if subscriber.peer_id != peer_id:
                peer_ids.append(subscriber.peer_id)

        self.__users_model.state_set(peer_id, State.DEFAULT)

        return [
            Command(
                command_type=CommandType.SEND_MESSAGE,
                peer_id=peer_id,
                keyboard=self.get_default_keyboard(peer_id),
                message='Рассылаю! Пиши, если что 😉',
            ),
            Command(
                command_type=CommandType.SEND_OUT_MESSAGE,
                peer_ids=peer_ids,
                message=text,
                attachment=new_attachments,
            )
        ]

    def command_send_out_info(self, message: Dict[str, Any], is_chat: bool, match_res: Optional[Match[str]]):
        peer_id = message['peer_id']
        subscribers = self.__users_model.subscription_info_users()
        commands = self.__send_out(message, subscribers)
        log.warning(f'{peer_id} разослал информацию')
        return commands

    def command_send_out_mem(self, message: Dict[str, Any], is_chat: bool, match_res: Optional[Match[str]]):
        peer_id = message['peer_id']
        subscribers = self.__users_model.subscription_mem_users()
        commands = self.__send_out(message, subscribers)
        log.info(f'{peer_id} разослал мем')
        return commands

    def command_send_out_notice(self, message: Dict[str, Any], is_chat: bool, match_res: Optional[Match[str]]):
        peer_id = message['peer_id']
        subscribers = self.__users_model.subscription_notice_users()
        commands = self.__send_out(message, subscribers)
        log.info(f'{peer_id} разослал информацию о дедлайне')
        return commands

    def command_select_notice_time(self, message: Dict[str, Any], is_chat: bool, match_res: Optional[Match[str]]):
        peer_id = message['peer_id']
        text = message['text']

        if re.fullmatch('(сейчас|now)', text, re.IGNORECASE | re.UNICODE):
            self.__users_model.state_set(peer_id, State.SEND_OUT_NOTICE)
        else:
            m = re.fullmatch(
                r'(2[01][0-9][0-9]|\*)\.'
                r'(0?[1-9]|1[0-2]|\*)\.'
                r'([0-3]?[0-9]|\*) '
                r'([0-2]?[0-9]|\*):'
                r'([0-5][0-9]|\*) ?'
                r'([1-7]|\*)?',
                text,
            )
            if m is None:
                return [
                    Command(
                        command_type=CommandType.SEND_MESSAGE,
                        peer_id=peer_id,
                        message='Какой-то непонятный формат 🙁. Попробуй ещё раз!',
                    ),
                ]

            time = []
            for word in m.groups():
                if word == '*':
                    time.append(word)
                elif word is not None:
                    time.append(str(int(word)))
            while len(time) < 6:
                time.append('*')

            self.__users_model.temp_data_set(peer_id, time)
            self.__users_model.state_set(peer_id, State.SAVE_NOTICE)

        log.debug(f'{peer_id} выбрал время дедлайна')

        keyboard = VkKeyboard()
        keyboard.add_button('Ничего', VkKeyboardColor.NEGATIVE)
        return [
            Command(
                command_type=CommandType.SEND_MESSAGE,
                peer_id=peer_id,
                keyboard=keyboard.get_keyboard(),
                message='Какую информацию о дедлайне хочешь разослать? Напиши "ничего", чтобы ничего не рассылать.',
            ),
        ]

    def command_save_notice(self, message: Dict[str, Any], is_chat: bool, match_res: Optional[Match[str]]):
        peer_id = message['peer_id']
        text = message['text']
        attachments = message['attachments']
        new_attachments = self.__messages_api.reply_attachments(attachments)
        time = self.__users_model.temp_data_get(peer_id)

        self.__notices_model.add(
            message=text,
            attachments=','.join(new_attachments),
            year=time[0],
            month=time[1],
            day=time[2],
            hour=time[3],
            minute=time[4],
            weekday=time[5],
        )

        self.__users_model.state_set(peer_id, State.DEFAULT)
        log.info(f'{peer_id} сохранил информацию о дедлайне')
        return [
            Command(
                command_type=CommandType.SEND_MESSAGE,
                peer_id=peer_id,
                keyboard=self.get_default_keyboard(peer_id),
                message='Разошлю! Пиши, если что 😉',
            ),
        ]

    def command_debug(self, message: Dict[str, Any], is_chat: bool, match_res: Optional[Match[str]]):
        peer_id = message['peer_id']
        user = self.__users_model.get(peer_id)
        state = self.__users_model.state_get(peer_id)
        return Command(
            command_type=CommandType.SEND_MESSAGE,
            peer_id=peer_id,
            message=f'''
                User:
                {user}

                State:
                {state}
            ''',
        )

    def parse(self, message: Dict[str, Any]):
        peer_id = message['peer_id']
        state = self.__users_model.state_get(peer_id)
        is_chat = MessagesApi.is_chat(peer_id)
        user = self.__users_model.get(peer_id)

        for command in self.__commands[state]:
            if (not is_chat and command.for_dialog) or (is_chat and command.for_chat):
                if not command.for_admin or user.is_admin:
                    match_res = command.check_pattern(message['clear_text'])
                    if match_res is not None:
                        return command.execute(message, is_chat, match_res)

        return None
