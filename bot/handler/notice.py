import datetime
import logging
from typing import Optional, List

from bot.command import Command, CommandType
from model.notices import NoticeModel
from model.user import UserModel

log = logging.getLogger('vk_bot.bot.handler.notice')


class NoticeHandler:
    def __init__(self, users_model: UserModel, notices_model: NoticeModel):
        self.__users_model = users_model
        self.__notices_model = notices_model

    def send_out_notices(self) -> Optional[List[Command]]:
        log.debug('Notices check started')

        now = datetime.datetime.now()
        notice_list = self.__notices_model.get(now.year, now.month, now.day, now.hour, now.minute, now.isoweekday())
        if len(notice_list) == 0:
            log.debug('There is no notices :с')
            return None

        peer_ids = []
        subscribers = self.__users_model.subscription_notice_users()
        for subscriber in subscribers:
            peer_ids.append(subscriber.peer_id)

        if len(peer_ids) == 0:
            log.debug('There is no subscribers :с')
            return None

        commands = []
        for notice in notice_list:
            commands.append(Command(
                command_type=CommandType.SEND_OUT_MESSAGE,
                peer_ids=peer_ids,
                message=notice.message,
                attachment=notice.attachments,
            ))
            log.info(f"Message '{notice.message}' was sent")
        return commands
