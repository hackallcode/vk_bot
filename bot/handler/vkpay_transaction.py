import logging
from typing import List

import math

from bot.command import Command, CommandType
from model.user import UserModel, State

log = logging.getLogger('vk_bot.bot.handler.vkpay_transaction')


class VkpayTransactionHandler:
    def __init__(self, vkpay_chat_id: int, users_model: UserModel):
        self.__chat_id = vkpay_chat_id
        self.__users_model = users_model

    @staticmethod
    def __get_thanks_message(amount: float) -> str:
        if amount < 130:
            return 'Спасибо! Хотя мог бы закинуть и побольше, тут даже на Биг Мак не хватит :с'

        elif amount < 240:
            return 'На Биг Мак, конечно, хватит, а вот на Биг Тейсти нет :с<br>Но спасибо!'

        elif amount < 500:
            return 'Ничего себе, даже на Биг Тейсти хватит! Спасибо!'

        elif amount < 1000:
            return 'Неплохо-неплохо! Но это еще не предел, можно и больше закинуть! И да, спасибо!'

        elif amount < 5000:
            return 'ТЫ ПРОСТО КАПИТАЛЬНЫЙ КРАСАВЧИК!!! Спасибо-спасибо-спасибо! Проси у одмена плюшек!'

        return 'Ты ебанулся? Спасибо, конечно, но с тобой что-то не то...'

    @staticmethod
    def __get_rubles_word(amount: int) -> str:
        last_one = amount % 10
        last_two = amount % 100

        if last_one == 1 and last_two != 11:
            return 'рубль'

        if 2 <= last_one <= 4 and not (11 <= last_two <= 14):
            return 'рубля'

        return 'рублей'

    def handle(self, obj) -> List[Command]:
        from_id = obj['from_id']
        description = obj['description']
        amount = round(obj['amount'] / 10) / 100
        if amount == round(amount):
            amount = round(amount)
        rubles_word = self.__get_rubles_word(math.trunc(amount))

        log.warning(f'User {from_id} sent {amount:.2f} rubles with description: {description}')
        self.__users_model.state_set(from_id, State.REPLY_VKPAY)

        return [
            Command(
                command_type=CommandType.SEND_MESSAGE,
                peer_id=from_id,
                message=self.__get_thanks_message(amount),
            ),
            Command(
                command_type=CommandType.SEND_MESSAGE,
                peer_id=from_id,
                message='Что хочешь написать от моего имени в ВАЖНОМ? Если ничего, то просто напиши "ничего".',
            ),
            Command(
                command_type=CommandType.SEND_MESSAGE,
                peer_id=self.__chat_id,
                message=f'Кто-то скинул мне {amount} {rubles_word}, поэтому я сейчас сюда напишу, что попросят 🙈',
            ),
        ]
