import logging
from typing import List

from api.messages import MessagesApi
from bot.command import Command, CommandType
from model.user import UserModel

log = logging.getLogger('vk_bot.bot.handler.wall_post_new')


class WallPostNewHandler:
    def __init__(self, users_model: UserModel):
        self.__users_model = users_model

    def handle(self, obj) -> List[Command]:
        attachment = MessagesApi.wall(obj['owner_id'], obj['id'], obj.get('access_key'))

        commands = []
        subscribers = self.__users_model.subscription_post_users()
        for subscriber in subscribers:
            commands.append(Command(
                command_type=CommandType.SEND_MESSAGE,
                peer_id=subscriber.peer_id,
                attachment=attachment
            ))

        return commands
