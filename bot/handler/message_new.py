import logging
from typing import Union, List, Optional

from api.messages import MessagesApi
from bot.command import CommandType, Command
from bot.handler.message.chat import ChatParser
from bot.handler.message.command import CommandParser
from model.notices import NoticeModel
from model.user import UserModel

log = logging.getLogger('vk_bot.bot.handler.message_new')


class MessageNewHandler:
    def __init__(self, messages_api: MessagesApi, users_model: UserModel, notices_model: NoticeModel,
                 important_chat_id: int, spam_chat_id: int, men_chat_id: int, vkpay_chat_id: int):
        self.__messages_api = messages_api
        self.__users_model = users_model

        self.__command_parser = CommandParser(
            messages_api,
            users_model,
            notices_model,
            important_chat_id,
            spam_chat_id,
            men_chat_id,
            vkpay_chat_id,
        )
        self.__chat_parser = ChatParser()

    def handle(self, obj) -> Optional[Union[Command, List[Command]]]:
        obj.message['clear_text'] = MessagesApi.clean(obj.message['text'])

        command_res = self.__command_parser.parse(obj.message)
        if command_res is not None:
            return command_res

        peer_id = obj.message['peer_id']
        text = obj.message['text']
        clear_text = obj.message['clear_text']

        if not self.__messages_api.is_for_bot(peer_id, text):
            return

        chat_res = self.__chat_parser.parse(clear_text, obj.message)
        if not chat_res:
            return

        return Command(
            command_type=CommandType.SEND_MESSAGE,
            peer_id=peer_id,
            message=chat_res,
            keyboard=self.__command_parser.get_default_keyboard(peer_id),
        )
