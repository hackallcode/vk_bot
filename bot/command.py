import logging
from enum import Enum
from typing import List

from api.messages import MessagesApi

log = logging.getLogger('vk_bot.bot.command')


class CommandType(Enum):
    SEND_MESSAGE = 1
    SEND_OUT_MESSAGE = 2
    MAKE_POST = 3


class Command:
    def __init__(self, command_type: CommandType, *args, **kwargs):
        self.type = command_type
        self.args = args
        self.kwargs = kwargs


class CommandHandler:
    def __init__(self, messages_api: MessagesApi):
        self.__handlers = {
            CommandType.SEND_MESSAGE: self.__send_message_handler,
            CommandType.SEND_OUT_MESSAGE: self.__send_out_message_handler,
            CommandType.MAKE_POST: self.__make_post_handler,
        }

        self.__messages_api = messages_api

    def __send_message_handler(self, *args, **kwargs):
        self.__messages_api.send_message(*args, **kwargs)

    def __send_out_message_handler(self, peer_ids, *args, **kwargs):
        chat_ids = []
        user_ids = []
        for peer_id in peer_ids:
            if MessagesApi.is_chat(peer_id):
                chat_ids.append(peer_id)
            else:
                user_ids.append(peer_id)

        commands = []
        if len(user_ids) > 0:
            commands.append(
                Command(
                    command_type=CommandType.SEND_MESSAGE,
                    user_ids=user_ids,
                    *args, **kwargs,
                )
            )
        for chat_id in chat_ids:
            commands.append(
                Command(
                    command_type=CommandType.SEND_MESSAGE,
                    peer_id=chat_id,
                    *args, **kwargs,
                )
            )

        self.exec_commands(commands)

    def __make_post_handler(self, *args, **kwargs):
        log.warning(f'Ooops, someone want to create post with args {args} and kwargs {kwargs}')

    def exec_command(self, command: Command):
        handler = self.__handlers.get(command.type, None)
        if handler is not None:
            # noinspection PyArgumentList
            handler(*command.args, **command.kwargs)
        else:
            log.warning(f'Ooops, someone want to execute command with unknown type {command.type}: {command}')

    def exec_commands(self, commands: List[Command]):
        for command in commands:
            self.exec_command(command)
