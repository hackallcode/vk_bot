import datetime as DT
import logging
import math
import smtplib
import threading
import time
import traceback
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from typing import Optional, Union, List

import pytz
from vk_api import VkApi
from vk_api.bot_longpoll import VkBotLongPoll, VkBotEventType, VkBotEvent

from api.messages import MessagesApi
from api.peer import PeerApi
from bot.command import CommandHandler, Command, CommandType
from bot.handler.birthday import BirthdayHandler
from bot.handler.message_edit import MessageEditHandler
from bot.handler.message_new import MessageNewHandler
from bot.handler.notice import NoticeHandler
from bot.handler.vkpay_transaction import VkpayTransactionHandler
from bot.handler.wall_post_new import WallPostNewHandler
from config import WAIT_TIMEOUT
from model.notices import NoticeModel
from model.user import UserModel

log = logging.getLogger('vk_bot.bot.api_handler')


def exec_commands(func):
    def wrapper(*args, **kwargs):
        self = args[0]
        commands = func(*args, **kwargs)
        if isinstance(commands, list):
            self._command_handler.exec_commands(commands)
        elif isinstance(commands, Command):
            self._command_handler.exec_command(commands)

    return wrapper


class ApiHandler:
    def __init__(self, group_id: int, token: str,
                 important_chat_id: int, spam_chat_id: int, men_chat_id: int, vkpay_chat_id: int,
                 from_email: str = None, crash_email: str = None, is_test: bool = False, admin_chat_ids: list = None):
        self.is_test = is_test

        self.__id = group_id
        self.__token = token
        self.__from_email = from_email or 'vk-bot@localhost'
        self.__crash_email = crash_email
        self.__admin_chat_ids = admin_chat_ids

        self.__session = VkApi(token=self.__token, api_version='5.115')
        self.__api = self.__session.get_api()
        self.__long_poll = VkBotLongPoll(vk=self.__session, group_id=self.__id, wait=WAIT_TIMEOUT)

        self.__messages_api = MessagesApi(self.__id, self.__api)
        self.__peer_api = PeerApi(self.__api)

        self.__users_model = UserModel(self.__id, self.__peer_api)
        self.__notices_model = NoticeModel(self.__id)

        self.__message_new_handler = MessageNewHandler(
            self.__messages_api,
            self.__users_model,
            self.__notices_model,
            important_chat_id,
            spam_chat_id,
            men_chat_id,
            vkpay_chat_id,
        )
        self.__message_edit_handler = MessageEditHandler(self.__messages_api)
        self.__wall_post_new_handler = WallPostNewHandler(self.__users_model)
        self.__vkpay_transaction_handler = VkpayTransactionHandler(vkpay_chat_id, self.__users_model)
        self.__birthday_handler = BirthdayHandler(self.__users_model)
        self.__notice_handler = NoticeHandler(self.__users_model, self.__notices_model)

        self._command_handler = CommandHandler(self.__messages_api)

    def __crash(self, event):
        text = f'Critical error after event: {event}\n{traceback.format_exc()[:-1]}'

        log.critical(text)

        if self.__crash_email:
            msg = MIMEMultipart()
            msg['Subject'] = 'VK Bot Crash'
            msg['To'] = self.__crash_email
            msg['From'] = self.__from_email
            msg.attach(MIMEText(text, 'plain', 'utf-8'))

            try:
                smtp_server = smtplib.SMTP('localhost')
                smtp_server.sendmail(self.__from_email, [self.__crash_email], msg.as_string().encode('ascii'))
                smtp_server.quit()
            except Exception as e:
                log.warning(f'Impossible to sent crash: {e}')

    @exec_commands
    def __start_handler(self) -> Optional[Union[Command, List[Command]]]:
        if self.__admin_chat_ids:
            return Command(
                command_type=CommandType.SEND_OUT_MESSAGE,
                peer_ids=self.__admin_chat_ids,
                message='Я жив!',
            )

    @exec_commands
    def __handle_cron(self, event: str) -> Optional[Union[Command, List[Command]]]:
        log.debug(f'new cron event: {event}')

        if event == 'birthday':
            return self.__birthday_handler.send_out_birthday()
        elif event == 'notice':
            return self.__notice_handler.send_out_notices()
        elif event == 'ping':
            return Command(
                command_type=CommandType.SEND_MESSAGE,
                peer_id=41644498,
                message='Ping!',
            )

        log.error(f'Unknown cron event: {event}')

    @staticmethod
    def __get_start_time(year=None, month=None, day=None, hour=0, minute=0, second=0, microsecond=0):
        now = DT.datetime.now()
        full = DT.datetime(
            year=now.year if year is None else year,
            month=now.month if month is None else month,
            day=now.day if day is None else day,
            hour=now.hour if hour is None else hour,
            minute=now.minute if minute is None else minute,
            second=now.second if second is None else second,
            microsecond=now.microsecond if microsecond is None else microsecond,
        )
        full = pytz.timezone('Europe/Moscow').localize(full)
        return full.timestamp()

    def __do_every(self, event, period, start=time.time() - 1):
        def g_tick():
            count = max(math.ceil((time.time() - start) / period), 0)
            while True:
                yield max(start + count * period - time.time(), 0)
                count += 1

        def main_loop():
            g = g_tick()
            while True:
                time.sleep(next(g))
                try:
                    self.__handle_cron(event)
                except Exception:
                    self.__crash(event)

        threading.Thread(target=main_loop, daemon=True).start()

    def __start_cron(self, debug: bool = False):
        self.__do_every(
            event='birthday',
            period=60 * 60 * 24,
            start=self.__get_start_time(hour=0, minute=1),
        )
        self.__do_every(
            event='notice',
            period=60,
            start=self.__get_start_time(second=5),
        )
        if debug:
            self.__do_every(
                event='ping',
                period=5,
                start=time.time() + 1,
            )

    @exec_commands
    def __handle_event(self, event: VkBotEvent) -> Optional[Union[Command, List[Command]]]:
        log.debug(f'new event: {event}')

        if event.type == VkBotEventType.MESSAGE_NEW:
            return self.__message_new_handler.handle(event.object)

        elif event.type == VkBotEventType.MESSAGE_EDIT:
            return self.__message_edit_handler.handle(event.object)

        elif event.type == VkBotEventType.WALL_POST_NEW:
            return self.__wall_post_new_handler.handle(event.object)

        elif event.type == VkBotEventType.VKPAY_TRANSACTION:
            return self.__vkpay_transaction_handler.handle(event.object)

        log.error(f'Unknown event: {event}')

    def __start_long_poll(self):
        while True:
            try:
                self.__start_handler()
                for event in self.__long_poll.listen():
                    try:
                        self.__handle_event(event)
                    except Exception:
                        self.__crash(event)
            except Exception as e:
                self.__crash(e)
                time.sleep(WAIT_TIMEOUT)

    def start(self, debug: bool = False):
        log.info(f'Server for group {self.__id} started')
        self.__start_cron(debug)
        self.__start_long_poll()
        log.info(f'Server for group {self.__id} stopped')
