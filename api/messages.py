import logging
import os
import random
import re
import sys
import uuid
from typing import List, Union

import wget
from vk_api import VkUpload
from vk_api.vk_api import VkApiMethod

from config import TEMP_PATH

log = logging.getLogger('vk_bot.api.messages')


class MessagesApi:
    BLACKLIST_REGEX = re.compile(r'\[club[0-9]+\|[^]]+\]', re.IGNORECASE | re.UNICODE)
    WHITELIST_REGEX = re.compile(r'(\[[^|]+\|[^]]+\]|[a-zа-я0-9 ?])', re.IGNORECASE | re.UNICODE)

    def __init__(self, group_id: int, api: VkApiMethod):
        self.__api = api
        self.__vk_upload = VkUpload(api)
        self.__group_id = group_id

    # Input messages

    @staticmethod
    def is_chat(peer_id: int):
        return peer_id >= 2000000000

    def is_for_bot(self, peer_id, message):
        if self.is_chat(peer_id):
            return message.find('club' + str(self.__group_id)) != -1

        return True

    @staticmethod
    def clean(message: str):
        message = message.replace('\n', ' ')
        message = message.replace('ё', 'е')
        message = MessagesApi.BLACKLIST_REGEX.sub(' ', message)
        message = ''.join(MessagesApi.WHITELIST_REGEX.findall(message))
        message = message.strip()
        return message

    # Output messages

    @staticmethod
    def get_random_id():
        return random.randint(1, sys.maxsize)

    @staticmethod
    def new_attachment(media_type: str, owner_id: int, media_id: int, access_key: str = None):
        return f'{media_type}{owner_id}_{media_id}' + ('' if access_key is None else f'_{access_key}')

    @staticmethod
    def photo(owner_id: int, media_id: int, access_key: str = None):
        return MessagesApi.new_attachment('photo', owner_id, media_id, access_key)

    @staticmethod
    def video(owner_id: int, media_id: int, access_key: str = None):
        return MessagesApi.new_attachment('video', owner_id, media_id, access_key)

    @staticmethod
    def audio(owner_id: int, media_id: int, access_key: str = None):
        return MessagesApi.new_attachment('audio', owner_id, media_id, access_key)

    @staticmethod
    def doc(owner_id: int, media_id: int, access_key: str = None):
        return MessagesApi.new_attachment('doc', owner_id, media_id, access_key)

    @staticmethod
    def wall(owner_id: int, media_id: int, access_key: str = None):
        return MessagesApi.new_attachment('wall', owner_id, media_id, access_key)

    @staticmethod
    def market(owner_id: int, media_id: int, access_key: str = None):
        return MessagesApi.new_attachment('market', owner_id, media_id, access_key)

    @staticmethod
    def poll(owner_id: int, media_id: int, access_key: str = None):
        return MessagesApi.new_attachment('poll', owner_id, media_id, access_key)

    def reload_attachment(self, attachment: dict):
        media_type = attachment['type']
        if media_type == 'photo':
            url = attachment[media_type]['sizes'][-1]['url']
            _, ext = os.path.splitext(url)
            ext = ext[1:]
        else:
            url = attachment[media_type]['url']
            ext = attachment[media_type]['ext']

        temp_name = os.path.join(TEMP_PATH, f'{uuid.uuid4()}.{ext}')
        wget.download(url, temp_name)

        if media_type == 'photo':
            new_attachment = self.__vk_upload.photo_messages(temp_name)
            new_attachment = new_attachment[0]
        else:
            new_attachment = self.__vk_upload.document_wall(
                temp_name, title=attachment[media_type]['title'],
                group_id=self.__group_id,
            )
            new_attachment = new_attachment[new_attachment['type']]

        os.remove(temp_name)
        return new_attachment

    def reply_attachment(self, attachment: dict):
        media_type = attachment['type']
        if media_type in ['photo', 'video', 'audio', 'doc', 'market']:
            owner_id = attachment[media_type]['owner_id']
        elif media_type in ['wall']:
            owner_id = attachment[media_type]['to_id']
        else:
            return ''
        media_id = attachment[media_type]['id']
        access_key = attachment[media_type].get('access_key', None)

        # Reload attachment
        if access_key and media_type in ['photo', 'doc']:
            new_attachment = self.reload_attachment(attachment)
            owner_id = new_attachment['owner_id']
            media_id = new_attachment['id']
            access_key = new_attachment.get('access_key', None)

        return self.new_attachment(media_type, owner_id, media_id, access_key)

    def reply_attachments(self, attachments: List[dict]):
        new_attachments = []
        for attachment in attachments:
            new_attachment = self.reply_attachment(attachment)
            if new_attachment:
                new_attachments.append(new_attachment)
        return new_attachments

    def send_message(self, peer_id: int = None, user_ids: List[int] = None, random_id: int = None,
                     message: str = None, attachment: Union[str, List[str]] = None, **kwargs):
        log.info(f'Message sent to {peer_id or user_ids}: `{message}`, {attachment}')
        self.__api.messages.send(
            peer_id=peer_id,
            user_ids=user_ids,
            message=message,
            attachment=attachment,
            random_id=MessagesApi.get_random_id() if random_id is None else random_id,
            **kwargs,
        )
