import logging

from vk_api.vk_api import VkApiMethod

log = logging.getLogger('vk_bot.api.peer')


class PeerApi:
    def __init__(self, api: VkApiMethod):
        self.__api = api

    def get_chats(self, peer_ids: int):
        chats = self.__api.messages.getConversationsById(
            peer_ids=peer_ids,
        )
        if chats['count'] == 0:
            return None
        if chats['count'] == 1:
            return chats['items'][0]
        return chats['items']

    def get_users(self, user_ids: int):
        users = self.__api.users.get(
            user_ids=user_ids,
        )
        if len(users) == 0:
            return None
        if len(users) == 1:
            return users[0]
        return users

    def get_peer_name(self, peer_id: int):
        if peer_id >= 2000000000:
            chat = self.get_chats(peer_id)
            return chat['chat_settings']['title']

        user = self.get_users(peer_id)
        return f"{user['first_name']} {user['last_name']}"
