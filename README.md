# VK Bot

This is a VK bot for integrating some group with chats.

## How to run

1. Install Python 3 and MySQL
2. Create DB for VK bot (check [script/db.sql](script/db.sql) file)
2. Create config file for bot from example:
`cp config.example.json config.json`
3. Run bot with script `main.py` 
(use `--test` flag to run only test groups)

## Structure

- Folder [bot](bot) contains base scripts
- Folder [bot/handler](bot/handler) contains scripts for
processing incoming events
- Folder [api](api) contains scripts for working with VK API
- Folder [model](model) contains scripts for working with the database
