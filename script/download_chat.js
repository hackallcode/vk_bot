let end = false;
let messages = [];

async function getMsgs(hash, peer_id, offset, count) {
    let res = await fetch("https://vk.com/dev", {
        "credentials": "include",
        "headers": {
            "accept": "*/*",
            "accept-language": "ru,en;q=0.9",
            "cache-control": "no-cache",
            "content-type": "application/x-www-form-urlencoded",
            "pragma": "no-cache",
            "sec-fetch-mode": "cors",
            "sec-fetch-site": "same-origin",
            "x-requested-with": "XMLHttpRequest"
        },
        "referrer": "https://vk.com/dev/messages.getHistory?params[offset]=" + offset + "&params[count]=" + count + "&params[peer_id]=" + peer_id + "&params[rev]=1&params[extended]=0&params[v]=5.103",
        "referrerPolicy": "no-referrer-when-downgrade",
        "body": "act=a_run_method&al=1&hash=" + hash + "&method=messages.getHistory&param_offset=" + offset + "&param_count=" + count + "&param_extended=0&param_peer_id=" + peer_id + "&param_rev=1&param_v=5.103",
        "method": "POST",
        "mode": "cors"
    });

    let data = await res.arrayBuffer();
    const dataView = new DataView(data);
    const decoder = new TextDecoder('windows-1251');
    data = decoder.decode(dataView);
    data = JSON.parse(data);
    data = data.payload[1][0];
    data = JSON.parse(data);
    return data;
}

function save(filename, data) {
    if (!data) {
        console.error('Console.save: No data');
        return;
    }

    if (!filename) filename = 'console.json';

    if (typeof data === "object") {
        data = JSON.stringify(data, undefined, 4)
    }

    let blob = new Blob([data], {type: 'text/json'}),
        e = document.createEvent('MouseEvents'),
        a = document.createElement('a');

    a.download = filename;
    a.href = window.URL.createObjectURL(blob);
    a.dataset.downloadurl = ['text/json', a.download, a.href].join(':');
    e.initMouseEvent('click', true, false, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
    a.dispatchEvent(e);
}

async function getAll(filename, hash, peer_id, offset, count) {
    console.log(offset + " of " + count);
    if (offset === 0) {
        end = false;
        messages = [];
    }
    if (offset >= count) {
        save(filename, messages);
        return;
    }
    let timeout = 1000;
    let data = await getMsgs(hash, peer_id, offset * 200, 200);
    if (!('error' in data)) {
        messages = messages.concat(data.response.items);
        offset++;
        timeout = 0;
    }
    setTimeout(getAll, timeout, filename, hash, peer_id, offset, count);
}

let hash = 'ENTER_HASH_HERE';
let chat_id = 2000000105;
let count = 5;

getAll('messages.json', hash, chat_id, 0, count)
    .catch((reason) => console.error('error:', reason));
