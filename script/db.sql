CREATE USER 'vk_bot'@'%' IDENTIFIED BY 'vk_bot';
CREATE DATABASE IF NOT EXISTS vk_bot;
GRANT ALL ON vk_bot.* TO 'vk_bot'@'%';
FLUSH PRIVILEGES;

USE vk_bot;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

CREATE TABLE `users`
(
    `id`              int(11)     NOT NULL AUTO_INCREMENT,
    `group_id`        int(11)     NOT NULL,
    `peer_id`         bigint(20)  NOT NULL,
    `updated`         timestamp   NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    `is_admin`        tinyint(1)  NOT NULL DEFAULT '0',
    `name`            varchar(32) NOT NULL DEFAULT '',
    `email`           varchar(254)         DEFAULT NULL,
    `birthday`        date                 DEFAULT NULL,
    `post_messages`   tinyint(1)  NOT NULL DEFAULT '0',
    `mem_messages`    tinyint(1)  NOT NULL DEFAULT '0',
    `notice_messages` tinyint(1)  NOT NULL DEFAULT '1',
    `info_messages`   tinyint(1)  NOT NULL DEFAULT '1',
    PRIMARY KEY (`id`)
) DEFAULT CHARSET = utf8;

ALTER TABLE `users`
    ADD UNIQUE KEY `group_id_peer_id_unique` (`group_id`, `peer_id`) USING BTREE,
    ADD KEY `group_id_peer_id_key` (`group_id`, `peer_id`) USING BTREE,
    ADD KEY `post_messages_key` (`post_messages`),
    ADD KEY `mem_messages_key` (`mem_messages`),
    ADD KEY `notice_messages_key` (`notice_messages`),
    ADD KEY `info_messages_key` (`info_messages`);

CREATE TABLE `congratulations`
(
    `id`       int(11)    NOT NULL AUTO_INCREMENT,
    `updated`  timestamp  NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    `sex`      tinyint(1) NOT NULL DEFAULT 0,
    `template` text       NOT NULL,
    PRIMARY KEY (`id`)
) DEFAULT CHARSET = utf8mb4;

INSERT INTO `congratulations` (`template`)
VALUES ('С днём рождения, {0}!');

CREATE TABLE `notices`
(
    `id`          int(11)    NOT NULL AUTO_INCREMENT,
    `group_id`    int(11)    NOT NULL,
    `updated`     timestamp  NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    `year`        varchar(4) NOT NULL DEFAULT '*',
    `month`       varchar(2) NOT NULL DEFAULT '*',
    `day`         varchar(2) NOT NULL DEFAULT '*',
    `hour`        varchar(2) NOT NULL DEFAULT '*',
    `minute`      varchar(2) NOT NULL DEFAULT '*',
    `weekday`     varchar(1) NOT NULL DEFAULT '*',
    `message`     text       NOT NULL,
    `attachments` text                DEFAULT NULL,
    PRIMARY KEY (`id`)
) DEFAULT CHARSET = utf8mb4;

ALTER TABLE `notices`
    ADD KEY `group_id_key` (`group_id`) USING BTREE;

COMMIT;
