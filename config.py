import json
import logging
import os
import random
import time

from model.db.congratulations import DbCongratulations
from model.db.connector import DbConnector
from model.db.notices import DbNotices
from model.db.users import DbUsers


def init_logger(loggers_config):
    formatters = {
        'simple': logging.Formatter(
            fmt='%(asctime)s <%(name)s:%(lineno)d> [%(levelname)s]: %(message)s',
            datefmt='%Y-%m-%d %H:%M:%S',
        )
    }

    handlers = []
    for logger in loggers_config:
        logger_type = logger.get('type', 'console')
        logger_level = logging.getLevelName(
            logger.get('level', 'INFO').upper()
        )
        logger_formatter = formatters.get(
            logger.get('format', 'simple').lower()
        )

        if logger_type == 'console':
            stream_handler = logging.StreamHandler()
            stream_handler.setFormatter(logger_formatter)
            stream_handler.setLevel(logger_level)
            handlers.append(stream_handler)
        elif logger_type == 'file':
            log_path = logger.get('path', 'log')
            if not os.path.isabs(log_path):
                log_path = os.path.join(os.getcwd(), log_path)
            os.makedirs(log_path, exist_ok=True)

            file_handler = logging.FileHandler(f'{log_path}/main.log', encoding='utf-8')
            file_handler.setFormatter(logger_formatter)
            file_handler.setLevel(logger_level)
            handlers.append(file_handler)
        else:
            raise Exception(f'Unknown logger type "{logger_type}"')

    # noinspection PyArgumentList
    logging.basicConfig(
        level=logging.NOTSET,
        handlers=handlers,
    )


def init_db(db_config):
    db_connector = DbConnector(
        host=db_config.get('host', 'localhost'),
        port=db_config.get('port', 3306),
        username=db_config.get('username', 'vk_bot'),
        password=db_config.get('password', 'vk_bot'),
        database=db_config.get('database', 'vk_bot'),
    )
    users_db = DbUsers(db_connector)
    congratulations_db = DbCongratulations(db_connector)
    notices_db = DbNotices(db_connector)
    return users_db, congratulations_db, notices_db


def init_temp_dir(temp_path):
    if not os.path.isabs(temp_path):
        temp_path = os.path.join(os.getcwd(), temp_path)
    os.makedirs(temp_path, exist_ok=True)
    return temp_path


def init_groups(groups_config):
    groups = []
    for group_config in groups_config:
        # Required params
        group = dict(
            group_id=group_config['group_id'],
            token=group_config['token'],
            important_chat_id=group_config['important_chat_id'],
            spam_chat_id=group_config['spam_chat_id'],
            men_chat_id=group_config['men_chat_id'],
            vkpay_chat_id=group_config['vkpay_chat_id'],
        )
        # Optional params
        if group_config.get('admin_chat_ids') is not None:
            group['admin_chat_ids'] = group_config['admin_chat_ids']
        if group_config.get('from_email') is not None:
            group['from_email'] = group_config['from_email']
        if group_config.get('crash_email') is not None:
            group['crash_email'] = group_config['crash_email']
        if group_config.get('is_test') is not None:
            group['is_test'] = group_config['is_test']
        groups.append(group)
    return groups


random.seed(time.time())

with open(os.path.join(os.getcwd(), 'config.json')) as fp:
    config_data = json.load(fp)

init_logger(config_data.get('loggers', []))

DB_USERS, DB_CONGRATULATIONS, DB_NOTICES = init_db(config_data.get('db', {}))
TEMP_PATH = init_temp_dir(config_data.get('temp_path', 'temp'))
GROUPS = init_groups(config_data.get('groups', []))
WAIT_TIMEOUT = config_data.get('timeout', 60)
