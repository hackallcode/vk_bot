import logging
from typing import Optional, List, Dict, Any

from model.db.connector import DbConnector

log = logging.getLogger('vk_bot.model.db.notices')


class DbNotices:
    def __init__(self, db: DbConnector):
        self.__db = db

    def add(
        self,
        group_id: int,
        message: str,
        attachments: Optional[str] = None,
        year: Optional[int] = None,
        month: Optional[int] = None,
        day: Optional[int] = None,
        hour: Optional[int] = None,
        minute: Optional[int] = None,
        weekday: Optional[int] = None,
    ) -> Optional[int]:
        return self.__db.modify(
            'INSERT INTO `notices` '
            '(`group_id`, `message`, `attachments`, `year`, `month`, `day`, `hour`, `minute`, `weekday`) '
            'VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)',
            (
                group_id,
                message,
                attachments,
                str(year) if year is not None else '*',
                str(month) if month is not None else '*',
                str(day) if day is not None else '*',
                str(hour) if hour is not None else '*',
                str(minute) if minute is not None else '*',
                str(weekday) if weekday is not None else '*',
            ),
        )

    def get(
        self,
        group_id: int,
        year: Optional[int] = None,
        month: Optional[int] = None,
        day: Optional[int] = None,
        hour: Optional[int] = None,
        minute: Optional[int] = None,
        weekday: Optional[int] = None,
    ) -> Optional[List[Dict[str, Any]]]:
        return self.__db.select(
            "SELECT * FROM `notices` "
            "WHERE `group_id` = %s "
            "AND (`year` = %s OR `year` = '*') "
            "AND (`month` = %s OR `month` = '*') "
            "AND (`day` = %s OR `day` = '*') "
            "AND (`hour` = %s OR `hour` = '*') "
            "AND (`minute` = %s OR `minute` = '*') "
            "AND (`weekday` = %s OR `weekday` = '*') ",
            (
                group_id,
                str(year) if year is not None else '*',
                str(month) if month is not None else '*',
                str(day) if day is not None else '*',
                str(hour) if hour is not None else '*',
                str(minute) if minute is not None else '*',
                str(weekday) if weekday is not None else '*',
            ),
        )

    def get_all(self, group_id: int) -> Optional[List[Dict[str, Any]]]:
        return self.__db.select("SELECT * FROM `notices` WHERE `group_id` = %s", (group_id,))

    def remove(self, notice_id: int) -> Optional[int]:
        return self.__db.modify('DELETE FROM `notices` WHERE `id` = %s', (notice_id,))
