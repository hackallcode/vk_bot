import logging
from typing import Optional, List, Dict, Any

from model.db.connector import DbConnector

log = logging.getLogger('vk_bot.model.db.congratulations')


class DbCongratulations:
    def __init__(self, db: DbConnector):
        self.__db = db

    def add(self, template: str, sex: int = 0) -> Optional[int]:
        return self.__db.modify(
            'INSERT INTO `congratulations` (`sex`, `template`) VALUES (%s, %s)',
            (sex, template),
        )

    def get_all(self, sex: int = 0) -> Optional[List[Dict[str, Any]]]:
        return self.__db.select(
            'SELECT * FROM `congratulations` WHERE `sex` = 0 or `sex` = %s',
            (sex,),
        )

    def remove(self, congratulation_id: int) -> Optional[int]:
        return self.__db.modify(
            'DELETE FROM `congratulations` WHERE `id` = %s',
            (congratulation_id,),
        )
