import datetime
import logging
from typing import Optional, List, Dict, Any

from model.db.connector import DbConnector

log = logging.getLogger('vk_bot.model.db.users')


class DbUsers:
    def __init__(self, db: DbConnector):
        self.__db = db

    def user_exists(self, group_id: int, peer_id: int) -> bool:
        res = self.__db.select(
            'SELECT `peer_id` FROM `users` WHERE `group_id` = %s AND `peer_id` = %s',
            (group_id, peer_id),
        )
        return len(res) > 0

    def user_add(self, group_id: int, peer_id: int, peer_name: str) -> Optional[int]:
        return self.__db.modify(
            'INSERT INTO `users` (`group_id`, `peer_id`, `name`) VALUES (%s, %s, %s)',
            (group_id, peer_id, peer_name),
        )

    def user_get(self, group_id: int, peer_id: int) -> Optional[List[Dict[str, Any]]]:
        return self.__db.select(
            'SELECT * FROM `users` WHERE `group_id` = %s AND `peer_id` = %s',
            (group_id, peer_id),
        )

    def user_subscription_set(self, group_id: int, peer_id: int, messages_type: str, value: int) -> Optional[int]:
        return self.__db.modify(
            f'UPDATE `users` SET `{messages_type + "_messages"}` = %s WHERE `group_id` = %s AND `peer_id` = %s',
            (value, group_id, peer_id),
        )

    def user_email_set(self, group_id: int, peer_id: int, value: str) -> Optional[int]:
        return self.__db.modify(
            'UPDATE `users` SET `email` = %s WHERE `group_id` = %s AND `peer_id` = %s',
            (value, group_id, peer_id),
        )

    def user_remove(self, group_id: int, peer_id: int) -> Optional[int]:
        return self.__db.modify(
            'DELETE FROM `users` WHERE `group_id` = %s AND `peer_id` = %s',
            (group_id, peer_id),
        )

    def subscription_users_get(self, group_id: int, messages_type: str) -> Optional[List[Dict[str, Any]]]:
        return self.__db.select(
            f'SELECT `peer_id` FROM `users` WHERE `{messages_type + "_messages"}` = 1 AND `group_id` = %s',
            (group_id,)
        )

    def birthday_users_get(self, group_id: int, date: datetime.date) -> Optional[List[Dict[str, Any]]]:
        return self.__db.select(
            'SELECT `peer_id`, `name` FROM `users` '
            'WHERE `group_id` = %s AND MONTH(`birthday`) = MONTH(%s) AND DAY(`birthday`) = DAY(%s)',
            (group_id, date, date)
        )
