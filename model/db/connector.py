import asyncio
import logging
import sys
import time
import traceback
from asyncio import Queue
from typing import Union, Optional, List, Dict, Any

import pymysql
from pymysql.cursors import DictCursor, Cursor

log = logging.getLogger('vk_bot.model.db.connector')

CONNECTION_ERROR_CODES = [1040, 1152, 1184, 1218, 2002, 2003, 2006, 2013, 2024, 2025, 2026, 2048]


class ConnectionPool:
    def __init__(self, host: str, username: str, password: str, database: str, port: int = None):
        self.__username = username
        self.__password = password
        self.__host = host
        self.__port = port
        self.__database = database

        self.__pool = None
        self.__max_pool_size = 5
        self.__time_to_sleep = 5
        self.__max_attempts = 3

        self.__initialize_pool()

    def __connect(self):
        log.debug(f'Connecting to {self.__host}:{self.__port}.{self.__database} by {self.__username}')
        return pymysql.connect(
            host=self.__host,
            port=self.__port,
            user=self.__username,
            passwd=self.__password,
            database=self.__database,
            charset='utf8mb4',
            cursorclass=DictCursor,
        )

    def __initialize_pool(self):
        log.debug(f'Creating a new pool of DB connections...')
        self.__pool = Queue(maxsize=self.__max_pool_size)
        attempts = 1
        while self.__pool.qsize() < self.__max_pool_size:
            try:
                self.__pool.put_nowait(self.__connect())
            except pymysql.OperationalError as error:
                if error.args[0] in CONNECTION_ERROR_CODES and attempts < self.__max_attempts:
                    log.error(
                        f"Cannot connect to DB! Retrying in {self.__time_to_sleep} seconds...\n"
                        f"{traceback.format_exc()[:-1]}"
                    )
                    attempts += 1
                    time.sleep(self.__time_to_sleep)
                else:
                    raise error

    def execute(self, *args, **kwargs):
        attempts = 0
        while attempts < self.__max_attempts:
            attempts += 1

            event_loop = asyncio.new_event_loop()
            asyncio.set_event_loop(event_loop)
            task = event_loop.create_task(self.__pool.get())
            event_loop.run_until_complete(task)
            conn = task.result()

            try:
                cursor = conn.cursor()
                cursor.execute(*args, **kwargs)
                conn.commit()
                self.__pool.put_nowait(conn)
                return cursor

            except pymysql.OperationalError as error:
                if error.args[0] in CONNECTION_ERROR_CODES:
                    log.warning(f'DB connection error: {sys.exc_info()[1]}')
                    self.__initialize_pool()
                else:
                    raise error

    def close(self):
        while self.__pool.qsize() > 0:
            conn = self.__pool.get_nowait()
            conn.close()


class DbConnector:
    def __init__(self, host: str, username: str, password: str, database: str, port: int = None):
        self.__pool = ConnectionPool(host, username, password, database, port)

    def __del__(self):
        self.__pool.close()

    def execute(self, query: str, args: Union[tuple, list, dict] = None) -> Optional[Cursor]:
        return self.__pool.execute(query, args)

    def select(self, query: str, args: Union[tuple, list, dict] = None) -> Optional[List[Dict[str, Any]]]:
        cursor = self.execute(query, args)
        if cursor is None:
            return None
        return cursor.fetchall()

    def modify(self, query: str, args: Union[tuple, list, dict] = None) -> Optional[int]:
        cursor = self.execute(query, args)
        if cursor is None:
            return None
        return cursor.rowcount
