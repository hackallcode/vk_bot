import logging
from collections import namedtuple
from typing import List, Optional

from config import DB_NOTICES

log = logging.getLogger('vk_bot.model.notice')

Notice = namedtuple('Notice', [
    'id',
    'message',
    'attachments',
    'year',
    'month',
    'day',
    'hour',
    'minute',
    'weekday',
], defaults=['', '*', '*', '*', '*', '*', '*'])


class NoticeModel:
    def __init__(self, group_id: int):
        self._group_id = group_id

    def add(
        self,
        message: str,
        attachments: Optional[str] = None,
        year: Optional[int] = None,
        month: Optional[int] = None,
        day: Optional[int] = None,
        hour: Optional[int] = None,
        minute: Optional[int] = None,
        weekday: Optional[int] = None,
    ) -> List[Notice]:
        return DB_NOTICES.add(self._group_id, message, attachments, year, month, day, hour, minute, weekday) > 0

    def get(
        self,
        year: Optional[int] = None,
        month: Optional[int] = None,
        day: Optional[int] = None,
        hour: Optional[int] = None,
        minute: Optional[int] = None,
        weekday: Optional[int] = None,
    ) -> List[Notice]:
        db_notices = DB_NOTICES.get(self._group_id, year, month, day, hour, minute, weekday)
        notices = []
        for obj in db_notices:
            notices.append(Notice(
                id=obj['id'],
                message=obj['message'],
                attachments=obj['attachments'],
                year=obj['year'],
                month=obj['month'],
                day=obj['day'],
                hour=obj['hour'],
                minute=obj['minute'],
                weekday=obj['weekday'],
            ))
        return notices

    def get_all(self) -> List[Notice]:
        db_notices = DB_NOTICES.get_all(self._group_id)
        notices = []
        for obj in db_notices:
            notices.append(Notice(
                id=obj['id'],
                message=obj['message'],
                attachments=obj['attachments'],
                year=obj['year'],
                month=obj['month'],
                day=obj['day'],
                hour=obj['hour'],
                minute=obj['minute'],
                weekday=obj['weekday'],
            ))
        return notices
