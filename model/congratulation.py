import logging
from collections import namedtuple
from enum import Enum
from typing import List

from config import DB_CONGRATULATIONS

log = logging.getLogger('vk_bot.model.congratulation')


class Sex(Enum):
    ANY = 0
    MALE = 1
    FEMALE = 2


Congratulation = namedtuple('Congratulation', [
    'id',
    'sex',
    'template',
], defaults=[0, 0, ''])


class CongratulationModel:
    @staticmethod
    def get_all(sex: Sex = Sex.ANY) -> List[Congratulation]:
        db_congratulations = DB_CONGRATULATIONS.get_all(sex.value)
        congratulations = []
        for obj in db_congratulations:
            congratulations.append(Congratulation(
                id=obj['id'],
                sex=Sex(obj['sex']),
                template=obj['template'],
            ))
        return congratulations
