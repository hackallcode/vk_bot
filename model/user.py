import datetime
import logging
from collections import namedtuple
from enum import Enum
from typing import List, Dict, Optional, Any

from api.peer import PeerApi
from config import DB_USERS

log = logging.getLogger('vk_bot.model.user')


class State(Enum):
    DEFAULT = 0
    ADMIN_MODE = 1
    SUBSCRIBE = 2
    UNSUBSCRIBE = 3
    REPLY_IMPORTANT = 4
    REPLY_SPAM = 5
    REPLY_MEN = 6
    REPLY_VKPAY = 7
    SEND_OUT_INFO = 8
    SEND_OUT_MEM = 9
    SEND_OUT_NOTICE = 10
    SELECT_NOTICE_TIME = 11
    SAVE_NOTICE = 12


User = namedtuple('User', [
    'peer_id',
    'group_id',
    'is_admin',
    'name',
    'email',
    'birthday',
    'has_post_subscription',
    'has_mem_subscription',
    'has_notice_subscription',
    'has_info_subscription',
], defaults=[0, False, '', '', None, False, False, False, False])


def create_user_if_not_exists(func):
    def wrapper(*args, **kwargs):
        self = args[0]
        group_id = self._group_id
        peer_id = kwargs.get('peer_id') or kwargs.get('from_id') or args[1]
        if not peer_id:
            raise Exception('No peer ID!')
        if not DB_USERS.user_exists(group_id, peer_id):
            peer_name = self._peer_api.get_peer_name(peer_id)
            DB_USERS.user_add(group_id, peer_id, peer_name)
        return func(*args, **kwargs)

    return wrapper


class UserModel:
    def __init__(self, group_id: int, peer_api: PeerApi):
        self._group_id = group_id
        self._peer_api = peer_api
        self.__user_states: Dict[int, State] = {}
        self.__user_temp_date: Dict[int, Any] = {}

    @create_user_if_not_exists
    def get(self, peer_id: int) -> User:
        user = DB_USERS.user_get(self._group_id, peer_id)[0]
        return User(
            group_id=user['group_id'],
            peer_id=user['peer_id'],
            is_admin=True if user['is_admin'] == 1 else False,
            name=user['name'],
            email=user['email'],
            birthday=user['birthday'],
            has_post_subscription=True if user['post_messages'] == 1 else False,
            has_mem_subscription=True if user['mem_messages'] == 1 else False,
            has_notice_subscription=True if user['notice_messages'] == 1 else False,
            has_info_subscription=True if user['info_messages'] == 1 else False,
        )

    @create_user_if_not_exists
    def subscription_mem_set(self, peer_id: int, value: bool) -> bool:
        return DB_USERS.user_subscription_set(self._group_id, peer_id, 'mem', 1 if value else 0) > 0

    def subscription_mem_users(self) -> List[User]:
        db_users = DB_USERS.subscription_users_get(self._group_id, 'mem')
        users = []
        for user in db_users:
            users.append(User(peer_id=user['peer_id']))
        return users

    @create_user_if_not_exists
    def subscription_post_set(self, peer_id: int, value: bool) -> bool:
        return DB_USERS.user_subscription_set(self._group_id, peer_id, 'post', 1 if value else 0) > 0

    def subscription_post_users(self) -> List[User]:
        db_users = DB_USERS.subscription_users_get(self._group_id, 'post')
        users = []
        for user in db_users:
            users.append(User(peer_id=user['peer_id']))
        return users

    @create_user_if_not_exists
    def subscription_notice_set(self, peer_id: int, value: bool) -> bool:
        return DB_USERS.user_subscription_set(self._group_id, peer_id, 'notice', 1 if value else 0) > 0

    def subscription_notice_users(self) -> List[User]:
        db_users = DB_USERS.subscription_users_get(self._group_id, 'notice')
        users = []
        for user in db_users:
            users.append(User(peer_id=user['peer_id']))
        return users

    @create_user_if_not_exists
    def subscription_info_set(self, peer_id: int, value: bool) -> bool:
        return DB_USERS.user_subscription_set(self._group_id, peer_id, 'info', 1 if value else 0) > 0

    def subscription_info_users(self) -> List[User]:
        db_users = DB_USERS.subscription_users_get(self._group_id, 'info')
        users = []
        for user in db_users:
            users.append(User(peer_id=user['peer_id']))
        return users

    @create_user_if_not_exists
    def state_set(self, peer_id: int, mode: State):
        self.__user_states[peer_id] = mode
        return True

    @create_user_if_not_exists
    def state_get(self, peer_id: int) -> State:
        return self.__user_states.get(peer_id, State.DEFAULT)

    @create_user_if_not_exists
    def email_set(self, peer_id: int, value: str) -> bool:
        return DB_USERS.user_email_set(self._group_id, peer_id, value) > 0

    @create_user_if_not_exists
    def temp_data_set(self, peer_id: int, value: Any):
        self.__user_temp_date[peer_id] = value
        return True

    @create_user_if_not_exists
    def temp_data_get(self, peer_id: int) -> Optional[Any]:
        return self.__user_temp_date.get(peer_id, None)

    def birthday_users_get(self, date: datetime.date) -> List[User]:
        db_users = DB_USERS.birthday_users_get(self._group_id, date)
        users = []
        for user in db_users:
            users.append(User(peer_id=user['peer_id'], name=user['name']))
        return users
